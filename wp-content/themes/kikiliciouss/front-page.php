<?php
/**
 * The template for displaying Front-Page
 *
 * Template name: Homepage
 *
 * @package Kikiliciouss
 */
get_header();

$mainBanner = get_field('main_banner');
$mainBannerMobile = get_field('main_banner_mobile');

$imageService = get_field('services_image');
$imageServiceMobile = get_field('services_image_mobile');
$titleService = get_field('services_title');
$textService = get_field('services_text');
$linkSerices = get_field('services_link');

$whoWeAreImage = get_field('who_we_are_image');
$whoWeAreTitle = get_field('who_we_are_title');
$whoWeAreLink = get_field('who_we_are_link');

$kristiHandsTitle = get_field('with_kristis_hands_title');
$kristiHandsImage = get_field('with_kristis_hands_image');
$kristiHandsLink = get_field('with_kristis_hands_link');


?>
<div class="container-fluid">
	<div class="row">
		<div class="col-12">
			<style>
				@media screen and (min-width: 992px){
					.main-banner{
						background-image: url('<?php echo $mainBanner['url']; ?>')
					}
				}
				@media screen and (max-width: 991px){
					.main-banner{
						background-image: url('<?php echo $mainBannerMobile['url']; ?>');
						background-size: cover;
					    background-position: center;
					    /*margin-bottom: -4em;*/
					}
				}@media screen and (max-width: 480px){
					.main-banner{
						margin-bottom: 0;
						background-size: contain;
						 background-position: top;
					}
				}
			</style>
			<section class="main-banner">
				<div class="main-banner-data">
					<div class="title">
						<h2><?php the_field('main_banner_header') ?></h2>
					</div>
					<p><?php the_field('main_banner_text') ?></p>
				</div>				
			</section>
		</div>
	</div>
	<div class="row recent-posts">
			<?php		
			$args = array( 'numberposts' => 4,'orderby' => 'post_date','order' => 'DESC','post_type' => 'post',"suppress_filters" => 0);
			$myposts = get_posts( $args );
			?>
				<div class="col-12">
					<?php if (ICL_LANGUAGE_CODE =='sq') { ?>
					<h2>Postimet e fundit</h2>
				<?php }
				else{?>
				<h2>Recent Posts</h2>
				<?php }?>
				</div>
				<?php foreach( $myposts as $post ) : setup_postdata($post); ?>
					<?php $image = get_the_post_thumbnail_url(); ?>
						<div class="col-6 col-md-6 col-lg-3 posts">
							<a href="<?php the_permalink(); ?>" class="post-url">
								<div class="post">
									<div class="post-image" style="background-image: url('<?php echo $image; ?>')">
										<img src="<?php //echo $image; ?>" alt="">
									</div>
									<div class="post-data">
										<h3 class="title">
											<?php the_title(); ?>
										</h3>
									</div>
								</div>
							</a>
						</div>
				<?php endforeach; ?>
		</div>
		<style>
				@media screen and (min-width: 992px){
					.image-service{
						background-image: url('<?php echo $imageService['url']; ?>')
					}
				}
				@media screen and (max-width: 991px){
					.image-service{
						background-image: url('<?php echo $imageServiceMobile['url']; ?>');
					}
				}
			</style>
		<div class="row services-row">
			<div class="col-12">
				<section class="services">
					<div class="image image-service"></div>
						<div class="data">
							<div class="title">
								<h2 class="title-h2">
									<?php echo $titleService; ?> 
								</h2>
							</div>
							<div class="content">
								<p class="content-paragraph">
									<?php echo $textService; ?>
								</p>
							</div>
						</div>
						<a href="<?php echo $linkSerices; ?>">
						<div class="arrow-right"></div>
						</a>
				</section>
			</div>
		</div>
		<div class="row who-we-are">
				<div class="col-md-12 col-lg-4">
					<section>
					<a href="<?php echo $whoWeAreLink ?>">
						<div class="column-who-we-are" style="background-image: url('<?php echo $whoWeAreImage['url']; ?>')">
							<div class="title">
								<h2 class="title-h2"><?php echo $whoWeAreTitle ?></h2>	
							</div>
							<div class="image-banner">
								<img src="<?php echo $whoWeAreImage['url'] ?>" alt="">
							</div>
						</div>
					</a>
					</section>
				</div>
				<div class="col-md-12 col-lg-8">
					<section>
					<a href="<?php echo $kristiHandsLink; ?>">
						<div class="column-who-we-are video">
							<div class="title">
								<h2 class="title-h2"><?php echo $kristiHandsTitle; ?></h2>	
							</div>
							<div class="image-banner" style="background-image: url('<?php echo $kristiHandsImage['url']; ?>');">
								<img src="<?php echo $kristiHandsImage['url']; ?>" alt="">
							</div>
						</div>
					</a>
				</section>
				</div>
		</div>
</div>
<?php
get_footer();