<?php
/**
 * The template for displaying 404 pages (not found)
 *
 * @link https://codex.wordpress.org/Creating_an_Error_404_Page
 *
 * @package Kikiliciouss
 */

get_header(); 
$bgImage = get_field('404_background','options');
$logoImage = get_field('404_logo','options');
?>

	<div class="container-fluid">
		<div class="row">
			<div class="col-lg-12 404-page"  style="background-image: url('<?php echo $bgImage['url'] ?>');">
				<div class="logo">
					<a href="<?php echo home_url(); ?>">
					<img src="<?php echo $logoImage['url']; ?>" alt="">
					</a>
				</div>
				<section class="404-section">
					<div class="data">
						<h1><?php the_field('404_header','options'); ?></h1>
						<p><?php the_field('404_description','options') ?></p>
						<a href="<?php echo home_url(); ?>"><?php the_field('404_redirect','options') ?></a>
					</div>
				</section>
			</div>
		</div>
	</div>

<?php get_footer();
