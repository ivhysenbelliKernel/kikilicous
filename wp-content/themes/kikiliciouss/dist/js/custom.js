(function($) {

    if($(window).width() < 991){
         $('.menu-item-has-children').click(function(){
            event.preventDefault();
            $(this).toggleClass('show-menu');
            $('.sub-menu').toggleClass('show-sub-menu');
        });
    }
    $('.sub-menu').click(function(){
        event.stopPropagation();
    })


})(jQuery);