<?php
/**
 * The template for displaying the footer
 *
 * Contains the closing of the #content div and all content after.
 *
 * @link https://developer.wordpress.org/themes/basics/template-files/#template-partials
 *
 * @package Kikiliciouss
 */

?>

	</div><!-- #content -->
	<div class="before-footer">
		<div class="instagram-feed">
			<?php echo do_shortcode('[instagram-feed num="8" cols="8"]'); ?>
			<p><?php echo (get_field('instagram_text','options'))?><a href="<?php echo (get_field('instagram_url','options')); ?>"> Instagram </a></p>
		</div>

		<div class="instagram-feed-mobile">
			<?php echo do_shortcode('[instagram-feed num="4" cols="4"]'); ?>
			<p><?php echo (get_field('instagram_text','options'))?><a href="<?php echo (get_field('instagram_url','options')); ?>"> Instagram </a></p>
		</div>
	</div>
	<footer class="site-footer">
		<div class="upper-footer">
			<div class="footer-logo">
				<?php $footerLogo = get_field('footer_logo','options'); ?>
				<img src="<?php echo $footerLogo['url'] ?>" alt="">
			</div>
			<div class="subscribe-social">
				<div class="subscribe">
					<?php 
					$formID = get_field('footer_contact_form_id','options');
					$formName = get_field('footer_contact_form_name','options');
					echo do_shortcode('[contact-form-7 id='.$formID.' title='.$formName.']'); 
					?>
				</div>
				<div class="social">
					<?php if( have_rows('social_links','options') ):
					 	// loop through the rows of data
					    while ( have_rows('social_links','options') ) : the_row();
					        // display a sub field value ?>
						<a href="<?php the_sub_field('social_link_url'); ?>"> <i class="fab fa-<?php the_sub_field('social_link_name'); ?>"></i></a>
					    <?php endwhile;
					endif; ?>
				</div>
			</div>
			<div class="footer-menu">
				<?php wp_nav_menu( array( 'theme_location' => 'footer' ) ); ?>
			</div>
		</div>
		<div class="lower-footer">
			<div class="rights">
				<p>&copy; <?php echo date("Y");   the_field('lower_footer',options); ?> </p>
			</div>
		</div>


	</footer><!-- #colophon -->
</div><!-- #page -->

<?php wp_footer(); ?>

</body>
</html>
