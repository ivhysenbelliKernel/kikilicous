<?php
/**
 * The header for our theme
 *
 * This is the template that displays all of the <head> section and everything up until <div id="content">
 *
 * @link https://developer.wordpress.org/themes/basics/template-files/#template-partials
 *
 * @package Kikiliciouss
 */

?>
<!doctype html>
<html <?php language_attributes(); ?>>
<head>
	<meta charset="<?php bloginfo( 'charset' ); ?>">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<link rel="profile" href="http://gmpg.org/xfn/11">

	<?php wp_head(); ?>
</head>

<body <?php body_class(); ?>>
<div id="page" class="site">
	<a class="skip-link screen-reader-text" href="#content"><?php esc_html_e( 'Skip to content', 'kikiliciouss' ); ?></a>
	<header id="masthead" class="site-header">
		<div class="top-header">
				<div class="search-form">
					<?php get_search_form(); ?>
				</div>
				<div class="mobile-logo">
						<?php $logo = get_custom_logo() ?>
						<?php echo $logo; ?>
				</div>
				<div class="languages">
				<?php $languages = icl_get_languages('skip_missing=0&orderby=ID&order=DESC'); 
				foreach ($languages as $language ) { ?>
					<a href="<?php echo $language['url']; ?>" class="<?php echo $language['tag']; if($language['active'] == '1') {echo " active"; } ?>"></a>
				<?php } ?>
			</div>
		</div>
			<nav id="site-navigation" class="main-navigation">
				<button class="menu-toggle" aria-controls="primary-menu" aria-expanded="false"></button>
				<?php
				wp_nav_menu( array(
					'theme_location' => 'menu-1',
					'menu_id'        => 'primary-menu',
				) );
				?>
			</nav><!-- #site-navigation -->
	</header><!-- #masthead -->
	<div id="content" class="site-content">
