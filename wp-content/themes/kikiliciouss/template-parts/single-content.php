<?php
$pfx_date = get_the_date( 'd/m/Y' ,$post->ID);
$image = get_the_post_thumbnail_url();
?>
<div class="header">
	<div class="header-title">
		<h2 class="title"><?php the_title(); ?></h2>
	</div>
	<div class="header-data">
		<div class="date">
			<h4><?php echo $pfx_date;?></h4>
		</div>
	</div>
	<div class="featured-image">
		<img class="single-header-image" src="<?php echo $image; ?>" alt="">
	</div>
</div>
<div class="content">
	<?php wpautop(the_content()); ?>
	<div class="tags">
		<?php the_tags(); ?>
	</div>
</div>