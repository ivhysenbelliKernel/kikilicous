<?php
$pfx_date = get_the_date( 'd/m/Y' ,$post->ID);
$image = get_the_post_thumbnail_url();
?>
<div class="search-data">
	<div class="search-image">
		<a href="<?php the_permalink(); ?>">
			<div class="featured-image">
				<img class="search-img" src="<?php echo $image; ?>" alt="">
			</div>	
		</a>
	</div>
	<div class="search-result-info">
		<a href="<?php the_permalink(); ?>">
			<div class="search-header-title">
				<h2 class="search-title"><?php the_title(); ?></h2>
			</div>
		</a>
		<div class="search-description">
			<div class="post-content">
				<?php echo '<p>'.wp_trim_words( get_the_content(), 80, '...' ).'</p>'; ?>
			</div>
			<div class="more-info">
				<div class="date">
					<h4><?php echo $pfx_date;?></h4>
				</div>
			</div>
		</div>
	</div>	
</div>
