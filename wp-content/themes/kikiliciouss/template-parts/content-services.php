
<div class="container">
	<div class="row">
		<div class="col-12">
			<p><?php the_field('header_text'); ?></p>
		</div>
	</div>
	<?php
	if( have_rows('row') ):
	    while ( have_rows('row') ) : the_row(); ?>
	<div class="row">
		<div class="col-lg-6">
			<div class="image">
				<?php $image = get_sub_field('image') ?>
				<img src="<?php  echo $image['url'] ?>" alt="">
			</div>
		</div>
		<div class="col-lg-6">
			<h3><?php the_sub_field('title_text') ?></h3>
			<p><?php the_sub_field('content') ?></p>
		</div>
	</div>
	<?php
	    endwhile;
	else :
	    // no rows found
	endif;
?>
</div>