<?php
/**
 * The template for displaying all single posts
 *
 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/#single-post
 *
 * @package Kikiliciouss
 */

get_header();
?>
<div id="content">
<div class="container-fluid">
	<div class="row">
		<div class="col-lg-9 col-single">
		<?php
		while ( have_posts() ) : the_post();

			get_template_part( 'template-parts/single-content', get_post_type() );

		endwhile; // End of the loop.
		?>
			<?php
//for use in the loop, list 5 post titles related to first tag on current post
$tags = wp_get_post_tags($post->ID);
if ($tags) { ?>
	<div class="row related-articles">
	<?php $first_tag = $tags[0]->term_id;
	$args=array(
	'tag__in' => array($first_tag),
	'post__not_in' => array($post->ID),
	'posts_per_page'=>3,
	'caller_get_posts'=>1
	);
	$my_query = new WP_Query($args);
	if( $my_query->have_posts() ) {
	while ($my_query->have_posts()) : $my_query->the_post(); ?>
		<?php $image = get_the_post_thumbnail_url(); ?>
	<div class="col-6 col-lg-4 related-col">
					<a href="<?php the_permalink(); ?>" class="post-url">
						<div class="post">
							<div class="post-image" style="background-image: url('<?php echo $image; ?>')">
								<img src="<?php //echo $image; ?>" alt="">
							</div>
							<div class="post-data">
								<h3 class="title">
									<?php the_title(); ?>
								</h3>
							</div>
						</div>
					</a>
				</div>
	<?php
	endwhile;
	}
	wp_reset_query();
}
?>
</div>
		</div>
		<div class="col-lg-3 sidebar-area">
			<?php get_sidebar(); ?>
			<div class="ads-image">
				<?php
					$adImage = get_field('widget_ad','options');
					$adImageMobile = get_field('widget_ad_mobile','options');
				?>
				<a href="<?php the_field('widget_ad_url','options') ?>">
					<img class="ad-custom-desktop" src="<?php echo $adImage['url']; ?>" alt="">
					<img class="ad-custom-mobile" src="<?php echo $adImageMobile['url']; ?>" alt="">
				</a>
			</div>
		</div>
		</div>
		
</div>
</div>
<?php
get_footer();
