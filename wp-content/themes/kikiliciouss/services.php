<?php
/**
 * The template for displaying Front-Page
 *
 * Template name: Services
 *
 * @package Kikiliciouss
 */
get_header(); ?>
<div class="container-fluid">
	<div class="row">
		<div class="col-lg-8 center">
			<p><?php the_field('header_text'); ?></p>
		</div>
	</div>
	<?php if(have_rows('row')): 
		$row_count = 1;
	while ( have_rows('row') ) : the_row(); ?>
		<div class="row service <?php if($row_count%2 == 0){echo "right-image";}else{echo 'left-image';} ?>">
			<div class="col-lg-6">
				<?php $image = get_sub_field('image'); ?>
				<div class="image-banner" style="background-image: url('<?php echo $image['url']; ?>')">
					<img src="<?php //echo $image['url']; ?>" alt="">
				</div>
			</div>
			<div class="col-lg-6 vertical-center">
				<div class="data">
					<div class="title">
						<h2 class="title-h2">
							<?php the_sub_field('title_text'); ?>
						</h2>
					</div>
					<div class="description">
						<p class="content-description">
							<?php the_sub_field('content') ?>
						</p>
					</div>
				</div>
			</div>
		</div>
	<?php 
	$row_count++;
	endwhile; 
	 endif; ?>
</div>
<?php 
get_footer();