<?php
get_header();
$page_object = get_queried_object();
$args = array ( 
	'category' 		 => $page_object->cat_ID, 
	'posts_per_page' => 12,
	'orderby'  		 => 'date',
	'paged' 		 => get_query_var( 'paged' ), );
$myposts = get_posts( $args ); 

$categories = get_the_category();
$category_id = $categories[0]->cat_ID;
?>

<div class="container-fluid">
	<div class="custom-search-form">
		<?php if (ICL_LANGUAGE_CODE =='sq') { ?>
		<form role="search" method="get" class="search-form" action="<?php echo home_url( '/' ); ?>">
		<input type="search" class="search-field" placeholder="Kërko" value="<?php echo esc_attr( get_search_query() ); ?>" name="s" title="<?php echo esc_attr_x( 'Search for:', 'label' ); ?>" />
	<?php
		// output all of our Categories
		// for more information see http://codex.wordpress.org/Function_Reference/wp_dropdown_categories
		$swp_cat_dropdown_args = array(
				'show_option_all'  => __( 'Të gjitha' ),
				'name'             => 'category',
				'include'		   => $category_id,
			);
		wp_dropdown_categories( $swp_cat_dropdown_args );
	}
	else { ?>
	<form role="search" method="get" class="search-form" action="<?php echo home_url( '/' ); ?>">
		<input type="search" class="search-field" placeholder="Search" value="<?php echo esc_attr( get_search_query() ); ?>" name="s" title="<?php echo esc_attr_x( 'Search for:', 'label' ); ?>" />
	<?php
		// output all of our Categories
		// for more information see http://codex.wordpress.org/Function_Reference/wp_dropdown_categories
		$swp_cat_dropdown_args = array(
				'show_option_all'  => __( 'All' ),
				'name'             => 'category',
				'include'		   => $category_id,
			);
		wp_dropdown_categories( $swp_cat_dropdown_args );
	} ?>
	<input type="submit" class="search-submit" value="Search" />
</form>
	</div>
	<div class="col-lg-8 center">
		<p><?php echo ($page_object->description); ?></p>
	</div>
	<div class="row margin-l-r">
		<?php foreach( $myposts as $post ) : setup_postdata($post); ?>
			<?php $image = get_the_post_thumbnail_url(); ?>
				<div class="col-6 col-sm-6 col-md-6 col-lg-3">
					<a href="<?php the_permalink(); ?>" class="post-url">
						<div class="post">
							<div class="post-image" style="background-image: url('<?php echo $image; ?>')">
								<img src="<?php //echo $image; ?>" alt="">
							</div>
							<div class="post-data">
								<h3 class="title">
									<?php the_title(); ?>
								</h3>
							</div>
						</div>
					</a>
				</div>
		<?php endforeach; ?>
		<div class="col-12">
			<div class="navigation">
				<?php echo paginate_links(); ?>
			</div>
		</div>
	</div>
</div>

<?php get_footer();	