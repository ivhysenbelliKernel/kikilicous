<?php
/**
 * The template for displaying search results pages
 *
 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/#search-result
 *
 * @package Kikiliciouss
 */

get_header();
?>
<div class="container">
	<div class="row">
		<div class="custom-search-form">
		<form role="search" method="get" class="search-form" action="<?php echo home_url( '/' ); ?>">
		<input type="search" class="search-field" placeholder="Kërko" value="<?php echo esc_attr( get_search_query() ); ?>" name="s" title="<?php echo esc_attr_x( 'Search for:', 'label' ); ?>" />
	<?php
		// output all of our Categories
		// for more information see http://codex.wordpress.org/Function_Reference/wp_dropdown_categories
		$swp_cat_dropdown_args = array(
				'show_option_all'  => __( 'Të gjitha' ),
				'name'             => 'category',
			);
		wp_dropdown_categories( $swp_cat_dropdown_args );
	?>
	<input type="submit" class="search-submit" value="Search" />
</form>
	</div>
	</div>
	<div class="row">

		<?php if ( have_posts() ) : ?>

			<div class="page-header">
				<h1 class="page-title">
					<?php
					/* translators: %s: search query. */
					printf( esc_html__( 'Rezultatet e Kërkimit: %s', 'kikiliciouss' ), '<span>' . get_search_query() . '</span>' );
					?>
				</h1>
			</div><!-- .page-header -->

			<?php
			/* Start the Loop */
			while ( have_posts() ) : the_post();
				/**
				 * Run the loop for the search to output the results.
				 * If you want to overload this in a child theme then include a file
				 * called content-search.php and that will be used instead.
				 */
				get_template_part( 'template-parts/content', 'search' );

			endwhile;

			the_posts_navigation();

		else :

			get_template_part( 'template-parts/content', 'none' );

		endif;
		?>
	</div>
</div>
<?php
get_footer();
