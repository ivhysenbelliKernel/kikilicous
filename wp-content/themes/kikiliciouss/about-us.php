<?php
/**
 * The template for displaying About Us
 *
 * Template name: About Us
 *
 * @package Kikiliciouss
 */
get_header();
$aboutUsImage = get_field('about_us_main_image');
$aboutUsImageMobile = get_field('about_us_main_image_mobile');
// var_dump($aboutUsImage);
?>

<div class="container-fluid">
	<div class="row">
		<div class="col-lg-10 center">
			<p><?php the_field('about_us_paragraph'); ?></p>
			<img src="<?php echo $aboutUsImage['url']; ?>" srcset="<?php echo $aboutUsImageMobile[url]; ?> 991w" alt="">
		</div>
	</div>
	<?php if(have_rows('about_us_personas')): 
		$row_count = 1;
	while ( have_rows('about_us_personas') ) : the_row(); ?>
		<div class="row about-us <?php if($row_count%2 == 0){echo "right-image";}else{echo 'left-image';} ?>">
			<div class="col-lg-6">
				<?php $image = get_sub_field('persona_image'); ?>
				<div class="image-banner" style="background-image: url('<?php echo $image['url']; ?>')">
					<img src="<?php //echo $image['url']; ?>" alt="">
				</div>
			</div>
			<div class="col-lg-6 vertical-center">
				<div class="data">
					<div class="title">
						<h2 class="title-h2">
							<?php the_sub_field('persona_name'); ?>
						</h2>
					</div>
					<div class="description">
						<p class="content-description">
							<?php the_sub_field('persona_description') ?>
						</p>
					</div>
				</div>
			</div>
		</div>
	<?php 
	$row_count++;
	endwhile; 
	 endif; ?>
</div>

<?php get_footer();