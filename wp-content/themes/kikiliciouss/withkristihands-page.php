<?php
/**
 * The template for displaying With Kristi's Hands
 *
 * Template name: With Kristi's hands
 *
 * @package Kikiliciouss
 */
get_header(); ?>
<div class="container-fluid">
	<div class="row">
		<div class="col-lg-10 center">
			<p><?php the_field('header_text'); ?></p>
		</div>
	</div>
	<div class="row section-cinema">
		<?php 
		// check if the repeater field has rows of data
			if( have_rows('youtube') ):
			 	// loop through the rows of data
			    while ( have_rows('youtube') ) : the_row(); ?>
			      <div class="col-md-6 col-lg-4">
			      	<div class="cinema-ratio">
			      		<?php the_sub_field('youtube_video'); ?>
			      	</div>
			      </div>
			<?php
			    endwhile;
			else :
			    // no rows found
			endif; ?>

		<div class="col-lg-10 center">
			<p><?php the_field('view_more')?></p>

			<div class="cinema-ratio-pl">
				<?php the_field('youtube_playlist'); ?>
			</div>
		</div>

	</div>
	<div class="row">
		
	</div>
</div>	
<?php
get_footer();