<?php
/**
 * The template for displaying Front-Page
 *
 * Template name: Contact Us
 *
 * @package Kikiliciouss
 */
$form_id = get_field('contact_form_id');
$form_name = get_field('contact_form_name');
get_header(); ?>
<div class="container-fluid">
	<div class="row">
		<div class="col-lg-12 center">
			<section class="contact-us">
			<div class="information">
				<p class="page-description"><?php the_field('header_text',false,false) ?></p>
			</div>
			<div class="contact-form">
			<?php echo do_shortcode( '[contact-form-7 id="'.$form_id.'" title="'.$form_name.'"]' ); ?>
			</div>
			</section>
		</div>
	</div>
</div>

<?php
get_footer();